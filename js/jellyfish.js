var _ = require('lodash');
var Octopus = require('./octopus'); // < - - important point number three

var Jellyfish = (function () {

  var canSting = (function () {
    return Octopus.getInkLevel() > 100;
  });

  return {
    canSting: canSting
  }

})();

module.exports = Jellyfish;
