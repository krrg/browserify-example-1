var _ = require('lodash');  // < - - important point number one

var Octopus = (function () {

  var inkLevel = 1000;

  var squeeze = function () {
    inkLevel -= 100;
  }

  var drink = function () {
    inkLevel += _.random(50, 100);
  }

  var getInkLevel = function () {
    return inkLevel;
  }

  return {
    squeeze: squeeze,
    drink: drink,
    getInkLevel: getInkLevel
  }

})();

module.exports = Octopus;  // < - - important point number two
